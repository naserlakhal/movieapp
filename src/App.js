import React, { useState, useEffect } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import MovieList from './components/MovieList';
import MovieListHeading from './components/MovieListHeading';
import SearchBox from './components/SearchBox';
import AddFavourites from './components/AddFavourites';
import RemoveFavourites from './components/RemovesFavourites';

const App = () => {
	const [movies, setMovies] = useState([]);
	const [searchValue, setSearchValue] = useState('');
	const [favourites, setFavourites] = useState([]);

	const getMovieRequest = async (searchValue) => {
		const url = `http://www.omdbapi.com/?s=${searchValue}&apikey=263d22d8`;

		const response = await fetch(url);
		const responseJson = await response.json();

		if (responseJson.Search) {
			setMovies(responseJson.Search);
		}
	};
	// useEffect(() => {
	// 	// localStorage.setItem('Favourites');
	// 	let x =localStorage.getItem('monChat');
	// 	console.log('x',x)
	// }, []);

	const StoreFavourites = (movie) => {
		localStorage.setItem('Favourites', JSON.stringify(movie));
	};

	const addFavouriteMovie = (movie) => {
		const newFavouriteList = [...favourites, movie];
		setFavourites(newFavouriteList);
		StoreFavourites(movie)
	};

	const removeFavouriteMovie = (movie) => {
		const newFavouriteList = favourites.filter(
			(favourite) => favourite.imdbID !== movie.imdbID
		);
		StoreFavourites(movie)


		setFavourites(newFavouriteList);
	};
	useEffect(() => {
		// let FavouritesString = localStorage.getItem('Favourites');
		// console.log('checking local storage', FavouritesString);
		// setFavourites(JSON.parse(FavouritesString));
		// let Favourites = JSON.parse(FavouritesString);
		// setFavourites(localStorage.getItem('Favourites'))
		// console.log('checking function',JSON.parse(x) )

		
	}, []);

	useEffect(() => {
		getMovieRequest(searchValue);
	}, [searchValue]);
	return (
		<div className='container-fluid movie-app'>
			<div className='row d-flex align-items-center mt-4 mb-4'>
				<MovieListHeading heading='Movies' />
				<SearchBox searchValue={searchValue} setSearchValue={setSearchValue} />
			</div>
			<div className='row'>
				<MovieList
					movies={movies}
					favouriteComponent={AddFavourites}
					handleFavouritesClick={addFavouriteMovie}
				/>
			</div>
			<div className='row d-flex align-items-center mt-4 mb-4'>
				<MovieListHeading heading='Favourites' />
			</div>
			<div className='row'>
				<MovieList
					movies={favourites}
					handleFavouritesClick={removeFavouriteMovie}
					favouriteComponent={RemoveFavourites}
				/>
			</div>
		</div>

	);
};

export default App;